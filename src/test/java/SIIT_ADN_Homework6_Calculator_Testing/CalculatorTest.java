package SIIT_ADN_Homework6_Calculator_Testing;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class CalculatorTest {

    private Calculator calculator;

    @BeforeEach
    void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testAdd() {
        calculator.add(100, Scale.cm);
        assertEquals(1, calculator.getResult(), "The result is not as expected");
        calculator.add(200, Scale.dm);
        assertEquals(21, calculator.getResult(), "The result is not as expected");
    }

    @Test
    public void testSubstract() {
        calculator.substract(100, Scale.cm);
        assertEquals(-1, calculator.getResult(), "The result is not as expected");
        calculator.substract(1200, Scale.mm);
        assertEquals(-2.2, calculator.getResult(), "The result is not as expected");
    }

    @Test
    public void testAdditionAndSubstraction() {
        calculator.add(100, Scale.dm);
        assertEquals(10, calculator.getResult(), "The result is not as expected");
        calculator.substract(130, Scale.cm);
        assertEquals(8.7, calculator.getResult(), "The result is not as expected");
        calculator.add(1650, Scale.mm);
        assertEquals(10.35, calculator.getResult(), "The result is not as expected");
        calculator.substract(0.022, Scale.km);
        assertEquals(-11.65, calculator.getResult(), "The result is not as expected");
    }

    @ParameterizedTest
    @ValueSource(doubles = {100})
    public void testSubstractWithParameter(double v) {
        calculator.substract(v, Scale.cm);
        assertEquals(-1, calculator.getResult(), "The result is not as expected");
        calculator.substract(v, Scale.mm);
        assertEquals(-1.1, calculator.getResult(), "The result is not as expected");

    }
}
