package SIIT_ADN_Homework6_Calculator_Testing;

import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.junit.platform.suite.api.SuiteDisplayName;

@Suite
    @SuiteDisplayName("My Tests")
    @SelectClasses({CalculatorTest.class})
    public class TestSuite {
    }

