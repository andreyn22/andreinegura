package SIIT_ADN_Homework6_Calculator_Testing;

public class Main {
    public static void main(String[] args) {

        Calculator c1 = new Calculator();
        c1.add(5,Scale.cm);
        c1.substract(2, Scale.m);
        c1.add(1, Scale.km);
        System.out.println("The result " + c1.toString());
        c1.setScale(Scale.dm);
        System.out.println("The result " + c1.toString());
        c1.setScale(Scale.km);
        System.out.println("The result " + c1.toString());
    }
}

//        DESCRIPTION Part I
//        write a calculator that is capable of computing a metric distance value from an expression
//        that contains different scales and systems.
//        output should be specified by the user
//        only addition and subtraction is allowed
//        implement the calculator and write UNIT TESTS that prove that the computations are correct.
//
//        NOTES
//        supported formats: mm, cm, dm, m, km
//
//        EXAMPLE
//        expression: 10 cm + 1 m - 10 mm = 1090 mm
//
//        DESCRIPTION Part II
//        for the previous implemented calculator, make sure you have the following tests
//        one parameterized test
//        one suite of tests which contains all existing ones

