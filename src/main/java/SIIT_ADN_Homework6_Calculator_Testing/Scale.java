package SIIT_ADN_Homework6_Calculator_Testing;

public enum Scale {

            m(1000),
            cm(10),
            mm(1),
            km(1000000),
            dm(100);

            private int scale;

            Scale(int scale) {
                this.scale = scale;
            }

    @Override
    public String toString() {
        return super.toString();
    }

}
