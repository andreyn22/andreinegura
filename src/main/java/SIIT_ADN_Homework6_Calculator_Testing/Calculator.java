package SIIT_ADN_Homework6_Calculator_Testing;

public class Calculator {

    private double result;
    private Scale scale;

    public Calculator() {
        this.result = 0;
        this.scale = Scale.m;
    }

    public double getResult() {
        return result;
    }
    public void setResult(double result) {
        this.result = result;
    }
    public Scale getScale() {
        return scale;
    }
    public void setScale(Scale scale) {
        Scale aux = this.scale;
        this.scale = scale;
        if (!aux.equals(scale)) {
            result = convert(result, aux);
        }
    }

    public void add(double v, Scale s) {
        if (s.equals(this.scale)) {
            result += v;
        } else {
            result += convert(v, s);
        }
    }

    public void substract(double v, Scale s) {
        if (s.equals(this.scale)) {
            result -= v;
        } else {
            result -= convert(v, s);
        }
    }

    @Override
    public String toString() {
        return result + scale.toString();
    }

    public double convert(double v1, Scale z) {
        switch (z.toString()) {
            case "mm":
                if (getScale().toString().equals("mm")) {
                    return v1;
                }
                if (getScale().toString().equals("cm")) {
                    return v1 / 10;
                }
                if (getScale().toString().equals("dm")) {
                    return v1 / 100;
                }
                if (getScale().toString().equals("m")) {
                    return v1 / 1000;
                }
                if (getScale().toString().equals("km")) {
                    return v1 / 1000000;
                }
                break;

            case "cm":
                if (getScale().toString().equals("mm")) {
                    return v1 * 10;
                }
                if (getScale().toString().equals("cm")) {
                    return v1;
                }
                if (getScale().toString().equals("dm")) {
                    return v1 / 10;
                }
                if (getScale().toString().equals("m")) {
                    return v1 / 100;
                }
                if (getScale().toString().equals("km")) {
                    return v1 / 100000;
                }
                break;

            case "dm":
                if (getScale().toString().equals("mm")) {
                    return v1 * 100;
                }
                if (getScale().toString().equals("cm")) {
                    return v1 * 10;
                }
                if (getScale().toString().equals("dm")) {
                    return v1;
                }
                if (getScale().toString().equals("m")) {
                    return v1 / 10;
                }
                if (getScale().toString().equals("km")) {
                    return v1 / 10000;
                }
                break;

            case "m":
                if (getScale().toString().equals("mm")) {
                    return v1 * 1000;
                }
                if (getScale().toString().equals("cm")) {
                    return v1 * 100;
                }
                if (getScale().toString().equals("dm")) {
                    return v1 * 10;
                }
                if (getScale().toString().equals("m")) {
                    return v1;
                }
                if (getScale().toString().equals("km")) {
                    return v1 / 1000;
                }
                break;

            case "km":
                if (getScale().toString().equals("mm")) {
                    return v1 * 1000000;
                }
                if (getScale().toString().equals("cm")) {
                    return v1 * 100000;
                }
                if (getScale().toString().equals("dm")) {
                    return v1 * 10000;
                }
                if (getScale().toString().equals("m")) {
                    return v1 * 1000;
                }
                if (getScale().toString().equals("km")) {
                    return v1;
                }
                break;
        }
        return 0;
    }

}


