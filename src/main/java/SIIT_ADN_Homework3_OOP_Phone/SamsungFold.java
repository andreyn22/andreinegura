package SIIT_ADN_Homework3_OOP_Phone;

public class SamsungFold extends Samsung {

    private static final String model= "Fold";

    @Override
    public String listContacts() {
        return "SamsungFold{" +
                "contacts='" + contacts + '\'' +
                '}';
    }
    @Override
    public String viewHistory() {
        return "SamsungFold{" +
                "calls='" + history + '\'' +
                '}';
    }
}
