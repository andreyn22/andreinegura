package SIIT_ADN_Homework4_OOP_Car;

public class VWGolf extends Volkswagen {

    public VWGolf(int fuelTankSize, String chassisNumber) {
        this.chassisNumber = chassisNumber;
        this.fuelTankSize = fuelTankSize;
        this.gears = 5;
        this.fuelType = "Diesel";
        this.tireSize = 15;
        this.availableFuel = 40;
        this.consumptionPer100Km = 5;
    }

    @Override
    public void start() {
        System.out.print(this.getClass().getName());
        super.start();
    }

    public void shiftGear(int shiftGear) {
        if (this.gears < shiftGear) {
            consumption += 0.03;
        }
        if (this.gears > shiftGear) {
            consumption -= 0.03;
        }

    }
}
