package SIIT_ADN_Homework4_OOP_Car;

public interface Vehicle {
  void start();
  void stop();
  void drive(double km);
  double getAvailableFuel();
}
