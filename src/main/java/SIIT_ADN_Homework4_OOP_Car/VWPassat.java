package SIIT_ADN_Homework4_OOP_Car;

public class VWPassat extends Volkswagen {

    public VWPassat(int fuelTankSize, String chassisNumber) {
        this.chassisNumber = chassisNumber;
        this.fuelTankSize = fuelTankSize;
        this.gears = 6;
        this.fuelType = "Gasoline";
        this.tireSize = 16;
        this.availableFuel = 48;
        this.consumptionPer100Km = 5.5;
    }

    @Override
    public void start() {
        System.out.print(this.getClass().getName());
        super.start();
    }

    public void shiftGear(int shiftGear) {
        if (this.gears < shiftGear) {
            consumption += 0.03;
        }
        if (this.gears > shiftGear) {
            consumption -= 0.03;
        }

    }
}