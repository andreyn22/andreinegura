package SIIT_ADN_Homework4_OOP_Car;

public class MSKlasse extends Mercedes {
    public MSKlasse(int fuelTankSize, String chassisNumber) {
        this.chassisNumber = chassisNumber;
        this.fuelTankSize = fuelTankSize;
        this.gears = 6;
        this.fuelType = "Diesel";
        this.tireSize = 18;
        this.availableFuel = 59;
        this.consumptionPer100Km = 8;
    }

    @Override
    public void start() {
        System.out.print(this.getClass().getName());
        super.start();
    }

    public void shiftGear(int shiftGear) {
        if (this.gears < shiftGear) {
            consumption += 0.05;
        }
        if (this.gears > shiftGear) {
            consumption -= 0.05;
        }
    }

}
