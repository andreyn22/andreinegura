package SIIT_ADN_Homework4_OOP_Car;

public class Main {
    public static void main(String[] args) {
        VWGolf car1 = new VWGolf(50, "oiqe0934hkkadsn");
        VWPassat car2 = new VWPassat(66, "dfsds093ssdfsdvs");
        Mercedes car3 = new MCKlasse(65, "fdgdf0934hkkadsn");
        MSKlasse car4 = new MSKlasse(80, "kmkjh0934hcksdjv");

        car1.start();
        car1.shiftGear(1);
        car1.drive(10);// drives 10 KMs
        car1.shiftGear(2);
        car1.drive(35);
        car1.shiftGear(3);
        car1.drive(48);
        car1.shiftGear(4);
        car1.drive(70);
        car1.shiftGear(4);
        car1.drive(85);
        car1.shiftGear(5);
        car1.drive(100);
        car1.shiftGear(4);
        car1.drive(110);
        car1.shiftGear(3);
        car1.drive(118);
        car1.stop();

        System.out.println();

        car4.start();
        car4.shiftGear(1);
        car4.drive(10);// drives 10 KMs
        car4.shiftGear(2);
        car4.drive(35);
        car4.shiftGear(3);
        car4.drive(48);
        car4.shiftGear(4);
        car4.drive(70);
        car4.shiftGear(4);
        car4.drive(85);
        car4.shiftGear(5);
        car4.drive(100);
        car4.shiftGear(4);
        car4.drive(110);
        car4.shiftGear(3);
        car4.drive(118);
        car4.stop();

    }
}

