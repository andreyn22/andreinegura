package SIIT_ADN_Homework4_OOP_Car;

public abstract class Car implements Vehicle {
    Integer fuelTankSize;
    String fuelType;
    Integer tireSize;
    Integer gears;
    double availableFuel;
    double consumptionPer100Km;
    double consumption;
    double distance;
    String chassisNumber;

    public void setFuelTankSize(Integer fuelTankSize) {
        this.fuelTankSize = fuelTankSize;
    }

    public Integer getTireSize() {
        return tireSize;
    }

    public void setTireSize(Integer tireSize) {
        this.tireSize = tireSize;
    }

    public void setGears(Integer gears) {
        this.gears = gears;
    }

    public void setAvailableFuel(double availableFuel) {
        this.availableFuel = availableFuel;
    }

    public void setConsumptionPer100Km(double consumptionPer100Km) {
        this.consumptionPer100Km = consumptionPer100Km;
    }

    public double getConsumption() {
        return consumption;
    }

    public void setConsumption(double consumption) {
        this.consumption = consumption;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public void start() {
        consumption = 0.01;
        System.out.println(" has started");
    }

    public void drive(double km) {
        consumption += consumptionPer100Km * km / 100 - (tireSize - 15) / 100;
        System.out.println("The consumption is " + consumptionPer100Km * km / 100 + " l");
        distance += km;
    }

    public void stop() {
        System.out.println("Estimated trip consumption " + consumptionPer100Km * distance / 100 + " l");
        System.out.println("The trip consumption was " + consumption + " l");
        System.out.println("Available fuel remaining " + getAvailableFuel() + " l");
    }


    public Integer getFuelTankSize() {
        return fuelTankSize;
    }

    public void setFuelTankSize(String fuelTankSize) {
        this.fuelTankSize = Integer.valueOf(fuelTankSize);
    }

    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(String fuelType) {
        this.fuelType = fuelType;
    }

    public Integer getGears() {
        return gears;
    }

    public void getGears(Integer gears) {
        this.gears = gears;
    }

    public double getConsumptionPer100Km() {
        return consumptionPer100Km;
    }

    public void setConsumptionPer100Km(Float consumptionPer100Km) {
        this.consumptionPer100Km = consumptionPer100Km;
    }

    public void shiftGear(int i) {
    }

    @Override
    public double getAvailableFuel() {
        return availableFuel - consumption;
    }

}
