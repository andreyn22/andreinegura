package SIIT_ADN_Homework7_IOEnums;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main( String[] args ) {


        System.out.println(); //Reading the csv file
        System.out.println("_________________Biathlon time results___________________");
        try(FileReader fileReader = new FileReader("SkiBiathlon.csv")){
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            String line = "";
            AthletesList model = new AthletesList();

            while((line = bufferedReader.readLine()) != null){
                String sLine = line.replace(" ", "");
                String[] arr = sLine.split(",");
                if(arr.length == 7){
                    try{
                        Athlete newAthlete = new Athlete(arr[0], arr[1], arr[2], arr[3], arr[4], arr[5], arr[6]);
                        model.addAthlete(newAthlete);
                    }catch (NullPointerException e){
                        System.out.println(e.getMessage() + " " + e.getCause());
                    }
                } else {
                    System.out.println("Please check if the file contain all the necessary data!");
                }
            }
            model.listAthletes();

            System.out.println();
            System.out.println("___________________Extra time__________________________");
            model.calculateExtraTime();

            System.out.println();
            System.out.println("________________Final time results_____________________");
            model.listAthletes();
            List<Athlete> l = new ArrayList<>();
            l.addAll(model.getLi());
            Collections.sort(l, new TimeComparator());

            System.out.println();
            System.out.println("________________Sorted time results______________________");
            for(Athlete a: l){
                System.out.println(a);
            }

            System.out.println();
            System.out.println("____________________Winner list________________________");
            //And the winners are: ->
            for(int i=0;i<l.size();i++){
                if(l.indexOf(l.get(i)) == 0){
                    System.out.println("Winner: " + l.get(i).getName() + " " + l.get(i).getTime());
                } else if(l.indexOf(l.get(i)) == 1){
                    System.out.println("2nd place: " + l.get(i).getName() + " " + l.get(i).getTime());
                } else if(l.indexOf(l.get(i)) == 2){
                    System.out.println("3rd place: " + l.get(i).getName() + " " + l.get(i).getTime());
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}