//Calculate the sum of the first 100 numbers higher than 0
// sum = sum + i;

package SIIT_ADN_Homework1_Exercises;
public class sum {
    public static void main(String[] args) {
        int num = 100, sum = 0;
        for(int i = 1; i <= num; ++i)
        {
            sum += i;
        }
        System.out.println("Sum = " + sum);
    }
}
