package SIIT_ADN_Homework5_ObjectContainers;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Person person1 = new Person("Ioana", 32);
        Person person2 = new Person("Clara", 40);
        Person person3 = new Person("Mara", 29);
        Person person4 = new Person("Marius", 32);

        Set<Person> personsAge = new TreeSet<>(new PersonAgeComparator());
        personsAge.add(person1);
        personsAge.add(person2);
        personsAge.add(person3);

        System.out.println("Print persons sorted by age: ");
        for (Person p : personsAge) {
            System.out.println(p);
        }

        System.out.println("");

        Set<Person> personsName = new TreeSet<>(new PersonNameComparator());
        personsName.add(person1);
        personsName.add(person2);
        personsName.add(person3);

        System.out.println("Print persons sorted by name: ");
        for (Person p : personsName) {
            System.out.println(p);
        }

        System.out.println("");

        Address address1 = new Address("Prague");
        Address address2 = new Address("Paris");

        List<Address> cyclingAddresses = new ArrayList<>();
        cyclingAddresses.add(address1);
        cyclingAddresses.add(address2);

        List<Address> joggingAddresses = new ArrayList<>();
        joggingAddresses.add(address1);
        joggingAddresses.add(address2);

        Hobby cyclingHobby = new Hobby("cycling", 2, cyclingAddresses);
        Hobby joggingHobby = new Hobby("jogging", 2, joggingAddresses);

        List<Hobby> hobbies = new ArrayList<>();
        hobbies.add(cyclingHobby);
        hobbies.add(joggingHobby);

        Map<Person, List<Hobby>> hobbyMapList = new HashMap<>();
        hobbyMapList.put(person4, hobbies);

        System.out.println("Print person with hobbies: ");
        System.out.println(hobbyMapList);
    }
}
