package SIIT_ADN_Homework5_ObjectContainers;

import java.util.List;

public class Hobby {
    private String hobby;
    private int frequency;
    private List<Address> addresses;

    public Hobby(String hobby, int frequency, List<Address> addresses) {
        this.hobby = hobby;
        this.frequency = frequency;
        this.addresses = addresses;
    }

    @Override
    public String toString() {
        return "Hobby{" +
                "hobby='" + hobby + '\'' +
                ", frequency=" + frequency +
                ", addresses=" + addresses +
                '}';
    }

}


//define for Hobby a class that contains
//        name of the hobby (String) – ex: cycling, swimming
//        frequency (int) – how many times a week they practice it
//        list of addresses where this hobby can be practiced (List<Address>)